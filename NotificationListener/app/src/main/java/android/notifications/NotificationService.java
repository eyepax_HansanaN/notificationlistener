package android.notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.support.v4.content.LocalBroadcastManager;

import java.io.ByteArrayOutputStream;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationService extends NotificationListenerService {

    Context context;
    String TAG = "hansana_test";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        if(Objects.equals(pack, "com.whatsapp")) {
            String ticker = "";
            if (sbn.getNotification().tickerText != null) {
                ticker = sbn.getNotification().tickerText.toString();
            }
            Bundle extras = sbn.getNotification().extras;
            String title = extras.getString("android.title");
            title = title.replaceAll("\\s+","");
            String text = extras.getCharSequence("android.text").toString();
            Log.d(TAG, "notification title: " + title);
            Log.d(TAG, "notification text: " + text);

            if (!Objects.equals(title, "WhatsApp")) {

                Log.v(TAG, "This notification title is not whatsapp");

                if (isValidPhoneNo(title)) {
                    Log.v(TAG, "this number is valid phone number");
                }
                title = title.substring(1);
                int id1 = extras.getInt(Notification.EXTRA_SMALL_ICON);
                Bitmap id = sbn.getNotification().largeIcon;

                Log.i("Package", pack);
                Log.i("Ticker", ticker);
                Log.i("Title", title);
                Log.i("Text", text);

                Intent msgrcv = new Intent("Msg");
                msgrcv.putExtra("package", pack);
                msgrcv.putExtra("ticker", ticker);
                msgrcv.putExtra("title", title);
                msgrcv.putExtra("text", text);
                if (id != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    id.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    msgrcv.putExtra("icon", byteArray);
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(msgrcv);
            }
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i("Msg","Notification Removed");
    }

    public boolean isValidPhoneNo(String phoneNumber) {
        String regex = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }
}

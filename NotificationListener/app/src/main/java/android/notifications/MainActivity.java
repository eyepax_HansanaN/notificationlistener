package android.notifications;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.notifications.model.MessageModel;
import android.notifications.model.ResponseModel;
import android.notifications.remote.ApiUtils;
import android.notifications.remote.MessageApiService;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity {

    ListView list;
    CustomListAdapter adapter;
    ArrayList<Model> modelList;

    String TAG = "hansana_test";

    private MessageApiService messageApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        modelList = new ArrayList<Model>();
        adapter = new CustomListAdapter(getApplicationContext(), modelList);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, new IntentFilter("Msg"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(
                        "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private BroadcastReceiver onNotice= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
           // String pack = intent.getStringExtra("package");
            String title = intent.getStringExtra("title");
            String text = intent.getStringExtra("text");
            //int id = intent.getIntExtra("icon",0);

            messageApiService = ApiUtils.getAPIService();

            Context remotePackageContext = null;
            try {
//                remotePackageContext = getApplicationContext().createPackageContext(pack, 0);
//                Drawable icon = remotePackageContext.getResources().getDrawable(id);
//                if(icon !=null) {
//                    ((ImageView) findViewById(R.id.imageView)).setBackground(icon);
//                }
                byte[] byteArray =intent.getByteArrayExtra("icon");
                Bitmap bmp = null;
                if(byteArray !=null) {
                    bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                }
                Model model = new Model();
                model.setName(title +" " +text);
                model.setImage(bmp);

                MessageModel messageModel = new MessageModel();
                messageModel.setMobileNumber(title);
                messageModel.setUserMessage(text);
                Log.v(TAG, messageModel.toString());

                if(messageModel.getMobileNumber() != null) {
                    sendApiCall(messageModel, context);
                }

                Log.v(TAG, "sendApiCall method finished!");

                if(modelList !=null) {
                    modelList.add(model);
                    adapter.notifyDataSetChanged();
                }else {
                    modelList = new ArrayList<Model>();
                    modelList.add(model);
                    adapter = new CustomListAdapter(getApplicationContext(), modelList);
                    list=(ListView)findViewById(R.id.list);
                    list.setAdapter(adapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void sendApiCall(MessageModel messageModel, final Context context) {
        messageApiService.sendMessage(messageModel).enqueue(new Callback<ResponseModel>() {
            @RequiresApi(api = Build.VERSION_CODES.DONUT)
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                if(response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                    Log.i(TAG, "response code: " + String.valueOf(response.code()));

                    String replyMessage = response.body().getReplyMessage();
                    Log.d(TAG, "Reply Message: " + replyMessage);
                    String replyNumber = response.body().getMobileNumber();
                    Log.d(TAG, "Reply Number: " + replyNumber);

                    // loading the WhatsApp with specific message to specific contact
                    PackageManager packageManager = context.getPackageManager();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //avoid setting flags as it will interfere with normal flow of event and history stack

                    try {
                        String encodedMessage = URLEncoder.encode(replyMessage, "UTF-8").replace("+", "%20");
                        String url = "https://api.whatsapp.com/send?phone="+ replyNumber +"&text=" + encodedMessage;
                        Log.i(TAG, "URL: " + url);
                        i.setPackage("com.whatsapp");
                        i.setData(Uri.parse(url));
                        if (i.resolveActivity(packageManager) != null) {
                            context.startActivity(i);
                        }

                        // TODO: 2/28/2018 Taping on the WhatsApp send button

                        new Timer().schedule(new TimerTask() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void run() {
                                // this code will be executed after 4 seconds

                                // this code will load the android home screen. this is because if we stay on the WhatsApp app notification will not be received.
                                Intent startMain = new Intent(Intent.ACTION_MAIN);
                                startMain.addCategory(Intent.CATEGORY_HOME);
//                                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(startMain);
                            }
                        }, 4000);

                    } catch (Exception e){
                        Log.e(TAG, "stack_trace: " + e.toString());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                Log.e(TAG, t.toString());
            }
        });
    }
}

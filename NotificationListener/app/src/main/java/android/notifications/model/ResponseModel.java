package android.notifications.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hansana_n on 2/21/2018.
 */

public class ResponseModel {
    @SerializedName("replyMessage")
    @Expose
    String replyMessage;

    @SerializedName("mobileNumber")
    @Expose
    String mobileNumber;

    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "replyMessage='" + replyMessage + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                '}';
    }
}

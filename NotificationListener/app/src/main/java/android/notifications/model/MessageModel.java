package android.notifications.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hansana_n on 2/20/2018.
 */

public class MessageModel {
    @SerializedName("mobileNumber")
    @Expose
    String mobileNumber;
    @SerializedName("userMessage")
    @Expose
    String userMessage;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    @Override
    public String toString() {
        return "MessageModel{" +
                "mobileNumber='" + mobileNumber + '\'' +
                ", userMessage='" + userMessage + '\'' +
                '}';
    }
}

package android.notifications.remote;

import android.notifications.model.MessageModel;
import android.notifications.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by hansana_n on 2/20/2018.
 */

public interface MessageApiService {
    @POST("/api/userMessage")
    //@FormUrlEncoded
    Call<ResponseModel> sendMessage(@Body MessageModel messageModel);
}

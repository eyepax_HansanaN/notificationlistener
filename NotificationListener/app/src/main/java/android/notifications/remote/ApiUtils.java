package android.notifications.remote;

/**
 * Created by hansana_n on 2/21/2018.
 */

public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "http://192.168.101.244:3000/";

    public static MessageApiService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(MessageApiService.class);
    }
}
